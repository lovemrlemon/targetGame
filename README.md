Thank you so much for taking time to read this! 

Please do share your thoughts on what I can improve in this project. :)


## Overview

This project is one of the introductory python assignments from Bergen University's
INF109 course.

graphics.py is the library used for the GUI provided by the INF109 course. The script
for the game is in targetGame.py file. 

It's a target hitting game. In order to win the game, the user must hit
the pink target that gets smaller each time. The aim is to hit 10 times in
maximum 10 seconds.

## Usage

The user can run the project using Python 3 by using the command below.

```python targetGame.py```

By default, the number of rounds in a game is 10. Though, it could be changed
easily by editing the ```numberOfRounds``` variable on line 7 in targetGame.py.

## Further Improvements

The game could be done by using a game loop where playing more than one game 
would be possible.

Also, a "Start" and "Game Over" screen could be added. 

