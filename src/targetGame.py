from random import randint
from graphics import *
from math import pow, sqrt
from datetime import datetime


NumberOfRounds = 10


def main():
    """runs the game"""
    win = createWindow()

    delta, penalty, death = runGameRound(win, NumberOfRounds)
    seconds = delta.total_seconds()

    if death == 1:
        print("Game over. Try again.")
    elif int(seconds) <= 10:
        print("Congrats!! You did it in ", int(seconds), " seconds. \nYou missed ", penalty, " times")
    else:
        excessTime = int(seconds) - 10
        print("You lost.. You overdid your time by ", excessTime, " seconds. \nYou missed ", penalty,
              " times.")

    win.close()

    return 0


def runGameRound(win, n):
    """This function draws a circle n times and for each time waits
    for the user to press inside the circle. Returns a pair
    containing the time used (timedelta) and the number of misses."""
    targetSize = 50

    penalty = 0
    start = datetime.now()
    death = 0

    for i in range(n):
        radius = targetSize - (5 * i)
        # radius = randint(20, 50)  # uncomment here and comment the upper line to make the game easier ^^

        center, antiCenter = centerCoordinates(radius)
        misses, death = drawAndCount(win, center, antiCenter, radius)
        penalty += misses

        if death == 1:
            break

    stop = datetime.now()
    delta = (stop - start)

    return delta, penalty, death


def drawAndCount(win, center, antiCenter, radius):
    """This function draws a red circle with center and radius as
    given in the given window and counts the amount of tries the user
    needs to hit inside it. Finally it undraws the circle and returns
    the number of misses the user made"""
    misses = 0
    death = 0

    cX, cY = center
    antiX, antiY = antiCenter

    target, antiTarget = drawTargets(win, center, antiCenter, radius)

    run = True
    while run:
        mousePoint = win.checkMouse()

        if mousePoint is not None:

            if isPointInside(cX, cY, radius, mousePoint):
                run = False
            elif isPointInside(antiX, antiY, radius, mousePoint):
                death = 1
                break
            else:
                misses += 1

    target.undraw()
    antiTarget.undraw()
    return misses, death


def centerCoordinates(radius):
    cX, cY = randomCoordinate(radius + 1, (winWidth() - radius - 1), radius + 1, (winHeight() - radius - 1))
    antiX, antiY = 0, 0
    run = True
    while run:
        antiX, antiY = randomCoordinate(radius + 1, (winWidth() - radius - 1), radius + 1, (winHeight() - radius - 1))
        if distance(antiX, cX, antiY, cY) > radius * 2:
            run = False
    center = (cX, cY)
    antiCenter = (antiX, antiY)

    return center, antiCenter


def drawTargets(win, center, antiCenter, radius):
    cX, cY = center
    antiX, antiY = antiCenter

    target = Circle(Point(cX, cY), radius)
    target.draw(win)
    target.setFill(color_rgb(204, 0, 102))
    target.setOutline(color_rgb(204, 0, 102))

    antiTarget = Circle(Point(antiX, antiY), radius)
    antiTarget.draw(win)
    antiTarget.setFill(color_rgb(0, 115, 153))
    antiTarget.setOutline(color_rgb(0, 115, 153))

    return target, antiTarget


def createWindow():
    win = GraphWin("GameStart!", winWidth(), winHeight())
    win.setCoords(0, 0, winWidth(), winHeight())
    win.setBackground(color_rgb(255, 204, 229))
    return win


def winWidth():
    return 600


def winHeight():
    return 400


def randomCoordinate(xMin, xMax, yMin, yMax):
    """assigns random coordinates for the center of the target"""
    x = randint(xMin, xMax)
    y = randint(yMin, yMax)
    return x, y


def sq(x):
    """returns x^2, x squared"""
    return pow(x, 2)


def distance(x1, x2, y1, y2):
    """returns the distance from (x1,y1), to (x2, y2)"""
    return sqrt(sq(x1 - x2) + sq(y1 - y2))


def isPointInside(centerX, centerY, radius, point):
    """checks if the mouse click is inside the target"""
    x = point.getX()
    y = point.getY()
    dist = distance(centerX, x, centerY, y)
    if dist <= radius:
        return True
    else:
        return False


main()
